<?php
require_once  'db.php';
error_reporting(1);
define("LOG", '/var/log/dlr.log');

class Dlr {
    public function write_log( $msg ){
        $date = Date('Y-m-d H:i:s');
        $msg = "\n || $date || $msg";
        if(file_exists(LOG)){
            file_put_contents( LOG, $msg, FILE_APPEND );
        }else{
            file_put_contents( LOG, $msg );
        }
    }
    public function raw_asf($saf_dlr)
    {
        $safcom_request = $saf_dlr['requestParam']['data'];
        $final = [];
        foreach($safcom_request as $item) {
            $final[$item['name']] = $item['value'];
        }
        return $final;
    }
    public function insert_to_db($clean_dlr,$table)
    {
       $reference_number =  $clean_dlr['ClientTransactionId'];
       $array_reference_id = explode('_',$reference_number);
       $blasts_id = $array_reference_id[0];
       $msisdn  = $clean_dlr['Msisdn'];
       $number_of_retries = $array_reference_id[2];
       $dlr_received_at = date("Y-m-d H:i:s", time());
       $status = 'InsufficientFunds';
       $created_at = date("Y-m-d H:i:s", time());
       $updated_at = date("Y-m-d H:i:s", time());
        $sql = "INSERT INTO $table(blasts_id,reference_number,msisdn,status,number_of_retries,dlr_received_at,created_at,updated_at)
                VALUES ($blasts_id,'$reference_number',$msisdn,'$status',$number_of_retries,'$dlr_received_at','$created_at','$updated_at')";
        var_dump($sql);
        $db = new Dba();
        $db->add_record($sql);
        return $clean_dlr;
    }
}