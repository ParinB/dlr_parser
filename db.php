<?php

class Dba {

    var $conn = NULL;
    var $insert_id;

    function __construct($db_host='127.0.0.1',$db_user='parin',$db_pass='infinity@double',$db_name='rt_ussd')
    {
        $this->conn = new mysqli($db_host,$db_user,$db_pass,$db_name);
    }
    function _clean_input($input)
    {
        return $this->conn->real_escape_string($input);
    }
    function setAutoCommitOff()
    {
        $this->conn->autocommit(FALSE);
    }
    function commitIt()
    {
        $this->conn->commit();
    }
    function rollItBack()
    {
        $this->conn->rollback();
    }
    function add_record($query)
    {
        try
        {

            $this->conn->autocommit(FALSE);
            $this->conn->query($query);
            $this ->insert_id = $this->conn->insert_id;
            $this->conn->commit();
            return TRUE;

        } catch (Exception $e) {

            $this->conn->rollItBack();
            return FALSE;

        }
        return FALSE;
    }
    function get_record($query){
        $result = $this->conn->query($query);
        $data = array();
        if($result) {
            while($row = $result->fetch_assoc()){
                $data[] = $row;
            }
        }
        return $data;
    }
    function get_last_insertid()
    {
        return $this->isert_id;
    }
    function update_record($query)
    {
        $status = FALSE;
        try {
            $this->conn->autocommit(FALSE);
            if ($this->conn->query($query)) {
                $status = true;
            }
        } catch (Exception $e) {

        }
    }
    }
