<?php
error_reporting(1);
require_once 'dlr.php';

//$saf_dlr = file_get_contents('php://input');
/*Example   Of  Sane  Saf  Dlr*/
$saf_dlr =  array(
    'requestId'=> '10185667358459530950',
    'requestTimeStamp' => '20200614135708',
    'requestParam' => array
     (
        'data' => array
        (
            0 => array
            (
                'name' => 'ClientTransactionId',
                'value' => '18_254705744566_0'
            ),
            1 => array
            (
                'name' => 'Type',
                'value' => 'DELIVERY_RECEIPT'
            ),
            2 => array
            (
                'name' => 'Description',
                'value' => 'DeliveredToTerminal'
            ),
            3 => array
            (
                'name' => 'Msisdn',
                'value' => '254705744566'
            )
        )
    ),
    'operation' => 'CP_NOTIFICATION'
);
$dlr =  new Dlr();
$clean_dlr = $dlr->raw_asf($saf_dlr);
$dlr->write_log("RequestId :: ".$saf_dlr['requestId']." :: ClientTransactionId :: ".$clean_dlr['ClientTransactionId']." :: DELIVERY_RECEIPT :: ".$clean_dlr['Description']." :: MSISDN :: ".$clean_dlr['Msisdn']);
if ($clean_dlr['Description'] == 'InsufficientFunds')
{
    $dlr->insert_to_db($clean_dlr,$table='insufficient_outboxes');
}else {
    $dlr->insert_to_db($clean_dlr,$table='other_status_outboxes');
}

